=head1 NAME

App::Parcimonie::Applet - systray applet object monitoring parcimonie's activities

=for Pod::Coverage TRUE FALSE

=cut

package App::Parcimonie::Applet;
use Moo;
use MooX::late;

use 5.10.0;

use Encode;

use Glib qw{TRUE FALSE};
use Gtk3 qw{-init};

use Net::DBus;
use Net::DBus::GLib;

use Locale::gettext;
use POSIX;
setlocale(LC_MESSAGES, "");
textdomain("parcimonie-applet");

use Time::Duration;
use Try::Tiny;

use namespace::clean;

with 'App::Parcimonie::Role::HasEncoding';
use MooX::StrictConstructor;


=head1 ATTRIBUTES

=cut

has 'verbose'  => (
    documentation => q{Use this option to get more output.},
    isa => 'Bool',
    is => 'ro',
    default => sub {
        exists $ENV{DEBUG} && defined $ENV{DEBUG} && $ENV{DEBUG}
    },
);

has 'icon_display_time' => (
    documentation => q{How many seconds the trayicon is displayed after key fetch.},
    isa           => 'Int',
    is            => 'ro',
    default       => 10,
);

has 'dbus_object' => (
    isa => 'Net::DBus::RemoteObject',
    is => 'rw',
    predicate => 'has_dbus_object',
);

has 'statusicon'  => (
    isa        => 'Gtk3::StatusIcon',
    is         => 'rw',
    lazy_build => 1,
 );

has 'monitorwin'  => (
    isa        => 'Gtk3::Window',
    is         => 'rw',
    lazy_build => 1,
);

has 'logbuffer'   => (
    isa        => 'Gtk3::TextBuffer',
    is         => 'rw',
    lazy_build => 1,
);


=head1 CONSTRUCTORS AND BUILDERS

=cut
sub BUILD {
    my $self = shift;
    $self->build_ui;
};

sub _build_monitorwin {
    my $self = shift;
    my $win = Gtk3::Window->new('toplevel');
    $win->set_title($self->encoding->decode(gettext('parcimonie log viewer')));
    $win->set_keep_above(1);
    $win->set_position('center');
    $win->signal_connect('delete_event' => sub { $win->hide; 1; });

    my $textview = Gtk3::TextView->new_with_buffer($self->logbuffer);
    $textview->set_editable(FALSE);
    $textview->set_cursor_visible(FALSE);
    $textview->set_wrap_mode('word');

    my $text_desc = Pango::FontDescription->new;
    $text_desc->set_family('Monospace');
    $textview->modify_font($text_desc);

    my $scrolled_win = Gtk3::ScrolledWindow->new;
    $scrolled_win->set_policy('automatic', 'automatic');
    $scrolled_win->add($textview);

    $win->add($scrolled_win);
    $win->set_default_size(800, 400);
    $win->set_size_request(100, 100);

    $win->signal_connect('key-press-event' => sub {
        my $twin = shift;
        my $event = shift;
        $win->hide if $event->key->{keyval} == Gtk3::Gdk::keyval_from_name('Escape');
    });

    return $win;
}

sub _build_logbuffer {
    my $self = shift;
    Gtk3::TextBuffer->new;
}

sub _build_statusicon {
    my $self = shift;
    my $icon = Gtk3::StatusIcon->new;

    $icon->set_visible(FALSE);
    $icon->set_from_stock('gtk-media-pause');

    my $menu = Gtk3::Menu->new;
    my $mlog = Gtk3::MenuItem->new_with_mnemonic($self->encoding->decode(gettext('View log')));
    $mlog->signal_connect('activate' => sub { $self->monitorwin->show_all });
    my $mexit = Gtk3::MenuItem->new_with_mnemonic($self->encoding->decode(gettext('Exit')));
    $mexit->signal_connect('activate' => sub { Gtk3->main_quit; });
    my $mabout = Gtk3::MenuItem->new_with_mnemonic($self->encoding->decode(gettext('About')));
    $mabout->signal_connect('activate' => sub { Gtk3->show_about_dialog(
        $self->monitorwin,
        'program-name' => 'Parcimonie applet',
        'license'      => q{This program is free software; you can redistribute it and/or modify it under the terms of either:

a) the GNU General Public License as published by the Free Software Foundation; either version 1, or (at your option) any later version, or

b) the "Artistic License" which comes with Perl.
},
        'wrap-license' => 1,
        'website'      => 'https://gaffer.boum.org/intrigeri/code/parcimonie/',
    )});
    $menu->append($mlog);
    $menu->append(Gtk3::SeparatorMenuItem->new);
    $menu->append($mexit);
    $menu->append(Gtk3::SeparatorMenuItem->new);
    $menu->append($mabout);

    $icon->signal_connect('popup-menu', sub {
        my ($ticon, $button, $time) = @_;
        my ($x, $y, $push) = Gtk3::StatusIcon::position_menu($menu, $ticon);
        $menu->show_all;
        $menu->popup(undef, undef, sub {($x, $y,$push)}, undef, $button, $time);
    });

    $icon->signal_connect('button-press-event' => sub {
        my $ticon = shift;
        my $event = shift;
        return unless $event->button == 1;
        if ($self->monitorwin->get_visible) {
            $self->monitorwin->hide;
        }
        else {
            $self->monitorwin->show_all;
        }
    });

    return $icon;
}


=head1 METHODS

=cut

sub debug {
    my $self = shift;
    my $mesg = shift;
    say STDERR $self->encoding->encode($mesg) if $self->verbose;
}

sub fatal {
    my $self      = shift;
    my $mesg      = shift;
    my $exit_code = shift;
    say STDERR $self->encoding->encode($mesg);
    exit($exit_code);
}

sub run {
    my $self = shift;
    for (1..30) {
        last if $self->dbus_connect;
        my $sleep = $_ * $_;
        warn("Cannot reach the daemon's D-Bus service. ",
             "Let' wait $sleep seconds before retrying.");
        sleep($sleep);
    }
    $self->fatal("Could not reach the parcimonie daemon's D-Bus service.", 16)
        unless $self->has_dbus_object;
    $self->debug("Connected to the daemon's D-Bus service.");
    $self->init_dbus_watcher;
    $self->debug("Entering main Gtk3 loop.");
    Gtk3->main;
}

sub build_ui {
    my $self = shift;
    $self->statusicon->set_visible(TRUE);
}

sub dbus_connect {
    my $self = shift;
    try {
        my $bus = Net::DBus::GLib->session;
        my $service = $bus->get_service("org.parcimonie.daemon");
        my $object = $service->get_object(
            "/org/parcimonie/daemon/object",
            "org.parcimonie.daemon"
        );
        $self->dbus_object($object);
        return 1;
    } catch {
        $self->debug("Could not connect to parcimonie daemon's D-Bus service.");
        return 0;
    };
}

sub init_dbus_watcher {
    my $self = shift;
    $self->dbus_object->connect_to_signal("FetchBegin", sub {
        $self->signal_fetch_begin_cb(@_);
    });
    $self->dbus_object->connect_to_signal("FetchEnd", sub {
        $self->signal_fetch_end_cb(@_);
    });
    $self->dbus_object->connect_to_signal("Sleeping", sub {
        $self->signal_sleeping_cb(@_);
    });
}

sub append_mesg {
    my $self = shift;
    my $mesg = shift;
    my $buffer = $self->logbuffer;
    $buffer->insert($buffer->get_end_iter, $mesg . "\n", -1);
}

sub signal_fetch_begin_cb {
    my $self  = shift;
    my $keyid = shift;

    # TRANSLATORS: OpenPGP key id
    my $mesg = $self->encoding->decode(
        sprintf(gettext('Fetching key %s...'), $keyid)
    );
    $self->debug($mesg);
    $self->statusicon->set_from_stock('gtk-refresh');
    $self->statusicon->set_tooltip_text($mesg);
    $self->append_mesg($mesg);
    $self->statusicon->set_visible(TRUE);
}

sub signal_fetch_end_cb {
    my $self    = shift;
    my $keyid   = shift;
    my $success = shift;
    my $gpgmesg = shift;
    $gpgmesg = $self->encoding->decode($gpgmesg);
    my $mesg =
        $success ? sprintf(
                     $self->encoding->decode(
                         # TRANSLATORS: OpenPGP key id
                         gettext('Successfully fetched key %s.')
                     ),
                     $keyid)
                 : sprintf(
                     $self->encoding->decode(
                         # TRANSLATORS: OpenPGP key id, error message
                         gettext('Failed to fetch key %s: %s.')
                     ),
                     $keyid, $gpgmesg
                   );
    my $new_pixmap =
        $success ? 'gtk-yes'
                 : 'gtk-no';

    $self->debug($mesg);
    $self->statusicon->set_from_stock($new_pixmap);
    $self->statusicon->set_tooltip_text($mesg);
    $self->append_mesg($mesg);
    $self->statusicon->set_visible(TRUE);
    Glib::Timeout->add(
        $self->icon_display_time * 1000,
        sub { 1 }
    );
}

sub signal_sleeping_cb {
    my $self     = shift;
    my $duration = shift;

    # TRANSLATORS: sleeping time
    my $mesg = $self->encoding->decode(
        sprintf(gettext('Sleeping for %s...'), duration($duration))
    );
    $self->debug($mesg);
    $self->statusicon->set_from_stock('gtk-media-pause');
    $self->statusicon->set_tooltip_text($mesg);
    $self->append_mesg($mesg);
    $self->statusicon->set_visible(TRUE);
}

no Moo;

1;
