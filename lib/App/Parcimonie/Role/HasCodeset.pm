=head1 NAME

App::Parcimonie::Role::HasCodeset - role to get the codeset being used

=head1 SYNOPSIS

    See App::Parcimonie::Role::HasEncoding source code.

=cut

package App::Parcimonie::Role::HasCodeset;

use Try::Tiny;

use Moo::Role; # Moo::Role exports all methods declared after it's "use"'d
use MooX::late;

use namespace::clean;

has 'codeset'  => (
    isa        => 'Str',
    is         => 'ro',
    lazy_build => 1,
);

sub _build_codeset {
    my $codeset;
    try {
        require I18N::Langinfo;
        I18N::Langinfo->import(qw(langinfo CODESET));
        $codeset = langinfo(CODESET());
    } catch {
        die "No default character code set configured.\nPlease fix your locale settings.";
    };
    $codeset;
}

no Moo::Role;
1; # End of App::Parcimonie::Role::HasCodeset
